# Movie Character API
Creates a datastore and interface to store and manipulate movie characters. 

#About the Program
This is a web API application that uses ASP.NET Core. Swagger is used for documentation and when starting the application it directs to the swagger page where you get an overview over endpoints and can test the different endpoints. There are also added a postman file which you can import to your postman program and run different API calls with tests included.

There are 5 different models for Franchise, Movie, Character, Actor, and a joining class for MovieCharacter. There are created DTOs, and automapping is used to implement automapping to and from the DTOs.