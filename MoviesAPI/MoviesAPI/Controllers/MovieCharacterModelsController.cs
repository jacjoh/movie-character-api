﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using MoreLinq;
using MoviesAPI.DTOs.Actor;
using MoviesAPI.DTOs.Character;
using MoviesAPI.DTOs.Movie;
using MoviesAPI.DTOs.MovieCharacter;
using MoviesAPI.Models;

namespace MoviesAPI.Controllers
{
    [Route("api/MovieCharacter")]
    [ApiController]
    public class MovieCharacterModelsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharacterModelsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the movie characters from the db
        /// </summary>
        /// GET: api/MovieCharacterModels
        /// <returns>List of movie characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<List<MovieCharacterDto>>>> GetMovieCharacters()
        {
            var movieCharacters = await _context.MovieCharacters
                .Include(mc => mc.Actor)
                .Include(mc => mc.Character)
                .Include(mc => mc.Movie)
                .ToListAsync();
            var movieCharactersDto = _mapper.Map<List<MovieCharacterDto>>(movieCharacters);
            return Ok(movieCharactersDto);
        }

        /// <summary>
        /// Gets a moviecharacter
        /// </summary>
        /// GET: api/MovieCharacterModels
        /// <returns>a movie character</returns>
        [HttpGet("{MovieId}&{CharacterId}&{ActorId}")]
        public async Task<ActionResult<IEnumerable<MovieCharacterDto>>> GetMovieCharacterModel(int MovieId, int CharacterId, int ActorId)
        {
            var movieCharacters = await _context.MovieCharacters
                .Where(mc => mc.ActorId == ActorId && mc.CharacterId == CharacterId && mc.MovieId == MovieId)
                .Include(mc => mc.Actor)
                .Include(mc => mc.Character)
                .Include(mc => mc.Movie)
                .FirstOrDefaultAsync();

            var movieCharactersDto = _mapper.Map<MovieCharacterDto>(movieCharacters);
            return Ok(movieCharactersDto);
        }

        /// <summary>
        /// Updates the movie character with Id's values
        /// </summary>
        /// <param name="MovieId"></param>
        /// <param name="CharacterId"></param>
        /// <param name="ActorId"></param>
        /// PUT: api/MovieCharacterModels/5
        /// <param name="mcPostDto"></param>
        /// <returns>204 No Content</returns>
        [HttpPut("{MovieId}&{CharacterId}&{ActorId}")]
        public async Task<IActionResult> PutMovieCharacterModel(int MovieId, int CharacterId, int ActorId, MovieCharacterPostDto mcPostDto)
        {
            //Composite key for actorid, characterid and movieid so must check that all the id's are present
            if (ActorId != mcPostDto.ActorId || CharacterId != mcPostDto.CharacterId || MovieId != mcPostDto.MovieId)
            {
                return BadRequest();
            }

            MovieCharacterModel mcModel = _mapper.Map<MovieCharacterModel>(mcPostDto);


            _context.Entry(mcModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterModelExists(MovieId, CharacterId, ActorId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new movie character entry to the db
        /// </summary>
        /// <param name="movieCharacterdto"></param>
        /// POST: api/MovieCharacterModels
        /// <returns>201 with the new movie characters and it's Id</returns>
        [HttpPost]
        public async Task<ActionResult<MovieCharacterPostDto>> PostMovieCharacterModel(MovieCharacterPostDto movieCharacterdto)
        {
            MovieCharacterModel mcModel = _mapper.Map<MovieCharacterModel>(movieCharacterdto);            
            _context.MovieCharacters.Add(mcModel);
            await _context.SaveChangesAsync();

            //The get method takes all the id's as argument, so must send them all to complete the response
            return CreatedAtAction("GetMovieCharacterModel", new { MovieId = mcModel.MovieId, CharacterId = mcModel.CharacterId, ActorId = mcModel.ActorId }, movieCharacterdto);
        }

        /// <summary>
        /// Deletes a movie character entry
        /// </summary>
        /// <param name="ActorId"></param>
        /// <param name="MovieId"></param>
        /// <param name="CharacterId"></param>
        /// DELETE: api/MovieCharacterModels/5
        /// <returns>200 with the deleted movie character</returns>
        [HttpDelete("{MovieId}&{CharacterId}&{ActorId}")]
        public async Task<ActionResult<MovieCharacterPostDto>> DeleteMovieCharacterModel(int MovieId, int CharacterId, int ActorId)
        {
            //All keys must be present, so that the right entry is deleted
            var movieCharacterModel = await _context.MovieCharacters
                .Where(mc => mc.ActorId == ActorId && mc.CharacterId == CharacterId && mc.MovieId == MovieId )
                .FirstOrDefaultAsync();

            if (movieCharacterModel == null)
            {
                return NotFound();
            }

            _context.MovieCharacters.Remove(movieCharacterModel);
            await _context.SaveChangesAsync();

            MovieCharacterPostDto mcdto = _mapper.Map<MovieCharacterPostDto>(movieCharacterModel);
            return Ok(mcdto);
        }

        /// <summary>
        /// Gets a movies character and the actors playing in them
        /// </summary>
        /// GET: api/MovieModels/5
        /// <param name="MovieId"></param>
        /// <returns>200 with a movie</returns>
        [HttpGet("CharactersForMovie/{MovieId}")]
        public async Task<ActionResult<List<CharacterForMovieDto>>> GetMovieCharactersActorModel(int MovieId)
        {
            var movieCharacterModel = await _context.MovieCharacters
                .Where(mc => mc.MovieId == MovieId)
                .Include(mc => mc.Character)
                .Include(mc => mc.Actor)
                .ToListAsync();

            if (movieCharacterModel == null)
            {
                return NotFound();
            }

            var characterAndActor = _mapper.Map<List<CharacterForMovieDto>>(movieCharacterModel);

            return Ok(characterAndActor);
        }

        /// <summary>
        /// Gets all the actors who have played a particular character
        /// </summary>
        /// GET: api/MovieModels/5
        /// <param name="CharacterId"></param>
        /// <returns>200 with actors</returns>
        [HttpGet("ActorsForCharacter/{CharacterId}")]
        public async Task<ActionResult<List<ActorDto>>> GetActorForCharacterModel(int CharacterId)
        {
            //Distinct to remove duplicates
            var actors = await _context.MovieCharacters.Where(mc => mc.CharacterId == CharacterId)
                .Select(mc => mc.Actor)
                .Distinct()
                .ToListAsync();

            if (actors.Count == 0)
            {
                return NotFound();
            }

            var actordto = _mapper.Map<List<ActorDto>>(actors);

            return Ok(actordto);
        }

        /// <summary>
        /// Gets all the characters for a particular actor
        /// </summary>
        /// GET: api/MovieModels/5
        /// <param name="ActorId"></param>
        /// <returns>200 with actors</returns>
        [HttpGet("CharactersForActor/{ActorId}")]
        public async Task<ActionResult<List<CharacterDto>>> GetCharacterForActorModel(int ActorId)
        {
            //Distinct to remove duplicates, can occur if an actor has played the same character in several movies
            var Characters = await _context.MovieCharacters.Where(mc => mc.ActorId == ActorId)
                .Select(mc => mc.Character)
                .Distinct()
                .ToListAsync();

            if (Characters.Count == 0)
            {
                return NotFound();
            }

            var characterdto = _mapper.Map<List<CharacterDto>>(Characters);

            return Ok(characterdto);
        }

        /// <summary>
        /// Gets all the movies an actor has played in
        /// </summary>
        /// GET: api/5/moviesForActor
        /// <param name="ActorId"></param>
        /// <returns>200 with Movies</returns>
        [HttpGet("MoviesForActor/{ActorId}")]
        public async Task<ActionResult<List<MovieDto>>> GetMoviesForActorModel(int ActorId)
        {
            var movies = await _context.MovieCharacters.Where(mc => mc.ActorId == ActorId)
                .Select(mc => mc.Movie)
                .ToListAsync();

            if (movies.Count == 0)
            {
                return NotFound();
            }

            var moviedto = _mapper.Map<List<MovieDto>>(movies);

            return Ok(moviedto);
        }

        //To check if a movie character exists, one have to check if the composite key are present in the db
        private bool MovieCharacterModelExists(int MovieId, int CharacterId, int ActorId)
        {
            return _context.MovieCharacters.Any(e => (e.ActorId == ActorId && e.CharacterId == CharacterId && e.MovieId == MovieId));
        }


    }
}
