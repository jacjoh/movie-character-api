﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.DTOs.Character;
using MoviesAPI.DTOs.Franchise;
using MoviesAPI.DTOs.Movie;
using MoviesAPI.Models;

namespace MoviesAPI.Controllers
{
    [Route("api/Franchise")]
    [ApiController]
    public class FranchiseModelsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public FranchiseModelsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all Franchises from the db
        /// </summary>
        /// GET: api/FranchiseModels
        /// <returns>200 with a List of franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<List<AllFranchisesDto>>>> GetFranchises()
        {
            var franchises = await _context.Franchises.ToListAsync();
            var franchiseDtos = _mapper.Map<List<AllFranchisesDto>>(franchises);
            return Ok(franchiseDtos);
        }

        /// <summary>
        /// Gets a specific franchise at specified Id
        /// </summary>
        /// GET: api/FranchiseModels/5
        /// <param name="id"></param>
        /// <returns>200 and a franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchiseModel(int id)
        {
            var franchiseModel = await _context.Franchises.FindAsync(id);
            var franchiseDto = _mapper.Map<FranchiseDto>(franchiseModel);


            if (franchiseDto == null)
            {
                return NotFound();
            }

            return Ok(franchiseDto);
        }

        /// <summary>
        /// Updates a franchise at specified Id
        /// </summary>
        /// PUT: api/FranchiseModels/5
        /// <param name="id"></param>
        /// <param name="franchiseDto"></param>
        /// <returns>204 No Content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchiseModel(int id, FranchiseDto franchiseDto)
        {
            FranchiseModel franchiseModel = _mapper.Map<FranchiseModel>(franchiseDto);

            if (id != franchiseModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchiseModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new franchise entry to the db
        /// </summary>
        /// POST: api/FranchiseModels
        /// <param name="franchiseDto"></param>
        /// <returns>201 Created at action with Id and franchise</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseDto>> PostFranchiseModel(FranchiseDto franchiseDto)
        {
            FranchiseModel franchiseModel = _mapper.Map<FranchiseModel>(franchiseDto);
            _context.Franchises.Add(franchiseModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchiseModel", new { id = franchiseModel.Id }, franchiseDto);
        }

        /// <summary>
        /// Deletes a franchise entry at specified Id
        /// </summary>
        /// <param name="id"></param>
        /// DELETE: api/FranchiseModels/5
        /// <returns>200 with the franchise deleted</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<FranchiseDto>> DeleteFranchiseModel(int id)
        {
            var franchiseModel = await _context.Franchises.FindAsync(id);
            if (franchiseModel == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchiseModel);
            await _context.SaveChangesAsync();

            FranchiseDto franchiseDto = _mapper.Map<FranchiseDto>(franchiseModel);


            return Ok(franchiseDto);
        }

        /// <summary>
        /// Get All movies for a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Franchise with additional movies</returns>
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<List<MovieFranchiseDto>>> GetFranchiseMoviesModel(int id)
        {
            //Includes franchises as the franchise name is included in the dto
            var franchiseMovies = await _context.Franchises.Where(f => f.Id == id)
                .SelectMany(f => f.Movies)
                .Include(m => m.Franchise)
                .ToListAsync();

            if (franchiseMovies == null)
            {
                return NotFound();
            }

            var moviedto = _mapper.Map<List<MovieFranchiseDto>>(franchiseMovies);

            return Ok(moviedto);
        }

        /// <summary>
        /// Get All characters for a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of characters</returns>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<List<CharacterDto>>> GetFranchiseCharacters(int id)
        {
            //Selects only the characters from the franchises
            var franchiseCharacters = await _context.Franchises.Where(f => f.Id == id)
                .SelectMany(f => f.Movies)
                .SelectMany(m => m.MovieCharacters)
                .Select(mc => mc.Character)
                .Distinct()
                .ToListAsync();

            if (franchiseCharacters == null)
            {
                return NotFound();
            }

            var characters = _mapper.Map<List<CharacterDto>>(franchiseCharacters);

            return Ok(characters);
        }

        private bool FranchiseModelExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }
    }
}
