﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.DTOs.Actor;
using MoviesAPI.Models;

namespace MoviesAPI.Controllers
{
    [Route("api/Actor")]
    [ApiController]
    public class ActorModelsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public ActorModelsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the actors stored in the database
        /// </summary>
        /// <remarks>
        /// Sample Request:
        /// GET /Actor
        /// </remarks>
        /// <returns>List of all actors</returns>
        /// <response code="200">
        /// Returns all actors  
        /// </response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetActors()
        {
            var actors = await _context.Actors.ToListAsync();
            var actordto = _mapper.Map<List<ActorDto>>(actors);
            return Ok(actordto);
        }

        /// <summary>
        /// Gets an actor with a specific Id
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// GET /Actor
        /// {
        ///     "Id": 1
        /// }
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Specific Actor</returns>
        /// <response code="200">
        /// Returns the wanted actor 
        /// </response>
        /// <response code="404">
        /// Did not find any actor with the Id
        /// </response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ActorDto>> GetActorModel(int id)
        {
            var actorModel = await _context.Actors.FindAsync(id);

            if (actorModel == null)
            {
                return NotFound();
            }

            var actordto = _mapper.Map<ActorDto>(actorModel);

            return Ok(actordto);
        }

        /// <summary>
        /// Updates a specific actors attributes
        /// </summary>
        /// PUT: api/ActorModels/5
        /// <param name="id"></param>
        /// <param name="actorDto"></param>
        /// <returns>204 No content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActorModel(int id, ActorDto actorDto)
        {
            ActorModel actorModel = _mapper.Map<ActorModel>(actorDto);
            
            if (id != actorModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(actorModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new actor entry to the db
        /// </summary>
        /// POST: api/ActorModels
        /// <param name="actorDto"></param>
        /// <returns>201 with the id and object created</returns>
        [HttpPost]
        public async Task<ActionResult<ActorModel>> PostActorModel(ActorDto actorDto)
        {
            ActorModel actorModel = _mapper.Map<ActorModel>(actorDto);
            _context.Actors.Add(actorModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActorModel", new { id = actorModel.Id }, actorDto);
        }

        /// <summary>
        /// Deletes an actor entry from the db
        /// </summary>
        /// DELETE: api/ActorModels/5
        /// <param name="id"></param>
        /// <returns>The removed actor</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ActorDto>> DeleteActorModel(int id)
        {
            var actorModel = await _context.Actors.FindAsync(id);
            if (actorModel == null)
            {
                return NotFound();
            }

            _context.Actors.Remove(actorModel);
            await _context.SaveChangesAsync();

            ActorDto actorDto = _mapper.Map<ActorDto>(actorModel);

            return Ok(actorDto);
        }

        private bool ActorModelExists(int id)
        {
            return _context.Actors.Any(e => e.Id == id);
        }
    }
}
