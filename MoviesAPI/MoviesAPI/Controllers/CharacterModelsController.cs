﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Writers;
using MoviesAPI.DTOs.Character;
using MoviesAPI.Models;

namespace MoviesAPI.Controllers
{
    [Route("api/Character")]
    [ApiController]
    public class CharacterModelsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public CharacterModelsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the characters from the db
        /// </summary>
        /// GET: api/CharacterModels
        /// <returns>List of characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<List<CharacterDto>>>> GetCharacterModels()
        {
            var characters = await _context.CharacterModels.ToListAsync();
            var chardto = _mapper.Map<List<CharacterDto>>(characters);
            return Ok(chardto);
        }

        /// <summary>
        /// Gets a specific character based on Id
        /// </summary>
        /// GET: api/CharacterModels/5
        /// <param name="id"></param>
        /// <returns>An character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacterModel(int id)
        {
            var characterModel = await _context.CharacterModels.FindAsync(id);

            if (characterModel == null)
            {
                return NotFound();
            }

            var chardto = _mapper.Map<List<CharacterDto>>(characterModel);

            return Ok(chardto);
        }

        /// <summary>
        /// Updates the attributes of a specific character with given id
        /// </summary>
        /// PUT: api/CharacterModels/5
        /// <param name="id"></param>
        /// <param name="characterdto"></param>
        /// <returns>204 No Content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacterModel(int id, CharacterDto characterdto)
        {
            CharacterModel characterModel = _mapper.Map<CharacterModel>(characterdto);

            if (id != characterModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(characterModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new character entry to the db
        /// </summary>
        /// POST: api/CharacterModels
        /// <param name="characterDto"></param>
        /// <returns>201 created at action with the Id and character</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterDto>> PostCharacterModel(CharacterDto characterDto)
        {
            CharacterModel characterModel = _mapper.Map<CharacterModel>(characterDto);
            _context.CharacterModels.Add(characterModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacterModel", new { id = characterModel.Id }, characterDto);
        }

        /// <summary>
        /// Deletes a character entry at specified Id
        /// </summary>
        /// DELETE: api/CharacterModels/5
        /// <param name="id"></param>
        /// <returns>200 and the deleted entry</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<CharacterModel>> DeleteCharacterModel(int id)
        {
            var characterModel = await _context.CharacterModels.FindAsync(id);
            if (characterModel == null)
            {
                return NotFound();
            }

            _context.CharacterModels.Remove(characterModel);
            await _context.SaveChangesAsync();

            CharacterDto characterDto = _mapper.Map<CharacterDto>(characterModel);

            return Ok(characterDto);
        }

        private bool CharacterModelExists(int id)
        {
            return _context.CharacterModels.Any(e => e.Id == id);
        }
    }
}
