﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.DTOs.Movie;
using MoviesAPI.Models;

namespace MoviesAPI.Controllers
{
    [Route("api/Movie")]
    [ApiController]
    public class MovieModelsController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;

        public MovieModelsController(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the movies in the db
        /// </summary>
        /// GET: api/MovieModels
        /// <returns>200 with a list of movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<List<MovieFranchiseDto>>>> GetMovies()
        {
            var movies = await _context.Movies
                .Include(m => m.Franchise)
                .ToListAsync();

            var moviesdto = _mapper.Map<List<MovieFranchiseDto>>(movies);
            return Ok(moviesdto);
        }

        /// <summary>
        /// Gets a movie with a specified Id
        /// </summary>
        /// GET: api/MovieModels/5
        /// <param name="id"></param>
        /// <returns>200 with a movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovieModel(int id)
        {
            var movieModel = await _context.Movies.FindAsync(id);
            var moviedto = _mapper.Map<MovieDto>(movieModel);

            if (moviedto == null)
            {
                return NotFound();
            }

            return Ok(moviedto);
        }

        /// <summary>
        /// Updates the attributes of a movie
        /// </summary>
        /// PUT: api/MovieModels/5
        /// <param name="id"></param>
        /// <param name="movieDto"></param>
        /// <returns>204 No Content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieModel(int id, MovieDto movieDto)
        {
            MovieModel movieModel = _mapper.Map<MovieModel>(movieDto);

            if (id != movieModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(movieModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new movie entry
        /// </summary>
        /// POST: api/MovieModels
        /// <param name="movieDto"></param>
        /// <returns>201 with the new movie and it's Id</returns>
        [HttpPost]
        public async Task<ActionResult<MovieDto>> PostMovieModel(MovieModel movieDto)
        {
            MovieModel movieModel = _mapper.Map<MovieModel>(movieDto);
            _context.Movies.Add(movieModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieModel", new { id = movieModel.Id }, movieDto);
        }

        /// <summary>
        /// Deletes a movie entry
        /// </summary>
        /// DELETE: api/MovieModels/5
        /// <param name="id"></param>
        /// <returns>200 with the deleted movie entry</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieDto>> DeleteMovieModel(int id)
        {
            var movieModel = await _context.Movies.FindAsync(id);
            if (movieModel == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movieModel);
            await _context.SaveChangesAsync();

            var moviedto = _mapper.Map<MovieDto>(movieModel);

            return Ok(moviedto);
        }

        private bool MovieModelExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }
    }
}
