﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.DTOs.MovieCharacter
{
    public class MovieCharacterPostDto
    {
        public string Picture { get; set; }
        public int MovieId { get; set; }
        public int CharacterId { get; set; }
        public int ActorId { get; set; }
    }
}
