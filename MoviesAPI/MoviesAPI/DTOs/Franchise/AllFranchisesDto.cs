﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.DTOs.Franchise
{
    public class AllFranchisesDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descripton { get; set; }
    }
}
