﻿using MoviesAPI.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.DTOs.Franchise
{
    public class FranchiseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descripton { get; set; }
        public ICollection<MovieDto> Movies { get; set; }
    }
}
