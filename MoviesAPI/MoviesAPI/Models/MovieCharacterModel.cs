﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class MovieCharacterModel
    {
        public string Picture { get; set; }

        //Joining class for movie, character, actor
        public int MovieId { get; set; }
        public MovieModel Movie { get; set; }
        public int CharacterId { get; set; }
        public CharacterModel Character { get; set; }
        public int ActorId { get; set; }
        public ActorModel Actor { get; set; }
    }
}
