﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class FranchiseModel
    {
        public int Id { get; set; }

        //Limits the length of the name to 50 characters
        [StringLength(50, ErrorMessage = "First name length cannot be more than 20 characters")]
        public string Name { get; set; }
        public string Descripton { get; set; }
        //Each franchise has many movies
        public ICollection<MovieModel> Movies { get; set; }
    }
}
