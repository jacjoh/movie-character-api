﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<FranchiseModel> Franchises { get; set; }
        public DbSet<MovieModel> Movies { get; set; }
        public DbSet<MovieCharacterModel> MovieCharacters { get; set; }
        public DbSet<CharacterModel> CharacterModels { get; set; }
        public DbSet<ActorModel> Actors { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Defines the keys in movie, actor, and character in the joining class
            modelBuilder.Entity<MovieCharacterModel>().HasKey(mc => new { mc.ActorId, mc.CharacterId, mc.MovieId });
            modelBuilder.Entity<MovieCharacterModel>()
                .HasOne(mc => mc.Movie)
                .WithMany(m => m.MovieCharacters)
                .HasForeignKey(mc => mc.MovieId);
            modelBuilder.Entity<MovieCharacterModel>()
                .HasOne(mc => mc.Actor)
                .WithMany(a => a.MovieCharacters)
                .HasForeignKey(mc => mc.ActorId);
            modelBuilder.Entity<MovieCharacterModel>()
                .HasOne(mc => mc.Character)
                .WithMany(m => m.MovieCharacters)
                .HasForeignKey(mc => mc.CharacterId);
            //Defines the connection between movies and franchises
            modelBuilder.Entity<FranchiseModel>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise);

            //Seeding data to the db
            //Franchises
            modelBuilder.Entity<FranchiseModel>().HasData(new FranchiseModel() { 
                Id = 1, 
                Name = "Marvel Universe", 
                Descripton = "American media franchise and shared universe centered on a series of superhero films" 
            });
            //Movies
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel() {
                Id = 2,
                MovieTitle = "Captain Marvel",
                Genre = "Action,Superhero,Science Fiction,Fantasy",
                ReleaseYear = 2019,
                Director = "Anna Boden,Ryan Fleck",
                Picture = "https://www.marvel.com/movies/captain-marvel",
                Trailer = "https://www.youtube.com/watch?v=Z1BCujX3pw8",
                FranchiseId = 1 
            });
            modelBuilder.Entity<MovieModel>().HasData(new MovieModel()
            {
                Id = 3,
                MovieTitle = "Spider-Man: Far From Home",
                Genre = "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen",
                ReleaseYear = 2019,
                Director = "Jon Watts",
                Picture = "https://en.wikipedia.org/wiki/Spider-Man:_Far_From_Home#/media/File:Spider-Man_Far_From_Home_poster.jpg",
                Trailer = "https://www.youtube.com/watch?v=Nt9L1jCKGnE",
                FranchiseId = 1
            });
            //Characters
            modelBuilder.Entity<CharacterModel>().HasData(new CharacterModel()
            {
                Id = 4,
                FullName = "Captain Marvel",
                Alias = "Ms. Marvel,Carol Denvers",
                Gender = "Woman",
                Picture = "https://en.wikipedia.org/wiki/Captain_Marvel_(film)#/media/File:Captain_Marvel_poster.jpg"
            });
            modelBuilder.Entity<CharacterModel>().HasData(new CharacterModel()
            {
                Id = 5,
                FullName = "Spider-Man",
                Alias = "Peter Parker",
                Gender = "Man",
                Picture = "https://upload.wikimedia.org/wikipedia/en/2/21/Web_of_Spider-Man_Vol_1_129-1.png"
            });
            modelBuilder.Entity<CharacterModel>().HasData(new CharacterModel()
            {
                Id = 6,
                FullName = "Nick Fury",
                Alias = "Colonel Nicholas Joseph Fury",
                Gender = "Man",
                Picture = "https://cdn3.movieweb.com/i/article/MnBIKHAdoI4tWtz67Q1p8fkwI7yAEG/1200:100/Marvel-Movies-Mcu-Samuel-L-Jackson-Legal-Obligations.jpg"
            });
            //Actors
            modelBuilder.Entity<ActorModel>().HasData(new ActorModel()
            {
                Id = 7,
                FirstName = "Thomas",
                OtherNames = "Stanley",
                LastName = "Holland",
                Gender = "Man",
                DateOfBirth = new DateTime(1996, 06, 01),
                PlaceOfBirth = "Kingston upon Thames",
                Biography = " A graduate of the BRIT School in London, he began his acting career on stage in the title role of Billy Elliot the Musical in " +
                "London's West End from 2008 to 2010.",
                Picture = "https://upload.wikimedia.org/wikipedia/commons/3/3c/Tom_Holland_by_Gage_Skidmore.jpg"
            });
            modelBuilder.Entity<ActorModel>().HasData(new ActorModel()
            {
                Id = 8,
                FirstName = "Samuel",
                OtherNames = "Leroy",
                LastName = "Jackson",
                Gender = "Man",
                DateOfBirth = new DateTime(1948, 12, 21),
                PlaceOfBirth = "Washington, D.C",
                Biography = "American actor and producer. Widely regarded as one of the most popular actors of his generation, the films in which he has " +
                "appeared have collectively grossed over $27 billion worldwide, making him the highest-grossing actor of all time.",
                Picture = "https://upload.wikimedia.org/wikipedia/commons/3/3c/Tom_Holland_by_Gage_Skidmore.jpg"
            });
            modelBuilder.Entity<ActorModel>().HasData(new ActorModel()
            {
                Id = 9,
                FirstName = "Brianne",
                OtherNames = "Sidonie",
                LastName = "Desaulniers",
                Gender = "Woman",
                DateOfBirth = new DateTime(1989, 10, 01),
                PlaceOfBirth = "Sacremento",
                Biography = "American actress and filmmaker. Noted for her supporting work in comedies when a teenager, she has since expanded to leading roles " +
                "in independent dramas and film franchises",
                Picture = "https://www.gstatic.com/tv/thumb/persons/230638/230638_v9_ba.jpg"
            });

            //LinkingTable
            //Captein Marvel
            modelBuilder.Entity<MovieCharacterModel>().HasData(new MovieCharacterModel() { 
                MovieId = 2,
                CharacterId = 4,
                ActorId = 9
            });
            modelBuilder.Entity<MovieCharacterModel>().HasData(new MovieCharacterModel()
            {
                MovieId = 2,
                CharacterId = 6,
                ActorId = 8
            });
            //Spider-man
            modelBuilder.Entity<MovieCharacterModel>().HasData(new MovieCharacterModel()
            {
                MovieId = 3,
                CharacterId = 5,
                ActorId = 7
            });
            modelBuilder.Entity<MovieCharacterModel>().HasData(new MovieCharacterModel()
            {
                MovieId = 3,
                CharacterId = 6,
                ActorId = 8
            });
        }
    }
}
