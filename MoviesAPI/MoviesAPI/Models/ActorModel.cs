﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class ActorModel
    {
        public int Id { get; set; }

        //Limits the string length to be less than the first argument, so it doesn't take up so much space        
        [StringLength(20, ErrorMessage = "First name length cannot be more than 20 characters")]
        public string FirstName { get; set; }

        [StringLength(40, ErrorMessage = "Other name length cannot be more than 20 characters")]
        public string OtherNames { get; set; }

        [StringLength(20, ErrorMessage = "Last name length cannot be more than 20 characters")]
        public string LastName { get; set; }

        [StringLength(20, ErrorMessage = "First name length cannot be more than 20 characters")]
        public string Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string PlaceOfBirth { get; set; }
        public string Biography { get; set; }
        public string Picture { get; set; }

        //An actor can play differnt characters in different movies
        public ICollection<MovieCharacterModel> MovieCharacters { get; set; }
    }
}
