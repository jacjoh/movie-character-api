﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class MovieModel
    {
        public int Id { get; set; }

        //Limits the length of movietitle to 70 character
        [StringLength(70, ErrorMessage = "First name length cannot be more than 20 characters")]
        public string MovieTitle { get; set; }
        public string Genre { get; set; }

        //Limits the range of the int to be bigger than 1887, as that was when the first movie came out
        [Range(1887, int.MaxValue, ErrorMessage = "Release year must be newer than 1887")]
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        
        //Each movie has many characters and actors
        public ICollection<MovieCharacterModel> MovieCharacters { get; set; }

        //The movies has one franchise
        public int FranchiseId { get; set; }
        public FranchiseModel Franchise { get; set; }
    }
}
