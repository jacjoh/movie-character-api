﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Models
{
    public class CharacterModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        
        //A character can be in different movies, and can be played by different actors
        public ICollection<MovieCharacterModel> MovieCharacters { get; set; }
    }
}
