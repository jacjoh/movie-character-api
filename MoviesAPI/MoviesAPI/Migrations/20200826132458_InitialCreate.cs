﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MoviesAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(maxLength: 20, nullable: true),
                    OtherNames = table.Column<string>(maxLength: 40, nullable: true),
                    LastName = table.Column<string>(maxLength: 20, nullable: true),
                    Gender = table.Column<string>(maxLength: 20, nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    PlaceOfBirth = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CharacterModels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Descripton = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(maxLength: 70, nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<int>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    Picture = table.Column<string>(nullable: true),
                    Trailer = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacters",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false),
                    ActorId = table.Column<int>(nullable: false),
                    Picture = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacters", x => new { x.ActorId, x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_CharacterModels_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "CharacterModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Biography", "DateOfBirth", "FirstName", "Gender", "LastName", "OtherNames", "Picture", "PlaceOfBirth" },
                values: new object[,]
                {
                    { 7, " A graduate of the BRIT School in London, he began his acting career on stage in the title role of Billy Elliot the Musical in London's West End from 2008 to 2010.", new DateTime(1996, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Thomas", "Man", "Holland", "Stanley", "https://upload.wikimedia.org/wikipedia/commons/3/3c/Tom_Holland_by_Gage_Skidmore.jpg", "Kingston upon Thames" },
                    { 8, "American actor and producer. Widely regarded as one of the most popular actors of his generation, the films in which he has appeared have collectively grossed over $27 billion worldwide, making him the highest-grossing actor of all time.", new DateTime(1948, 12, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Samuel", "Man", "Jackson", "Leroy", "https://upload.wikimedia.org/wikipedia/commons/3/3c/Tom_Holland_by_Gage_Skidmore.jpg", "Washington, D.C" },
                    { 9, "American actress and filmmaker. Noted for her supporting work in comedies when a teenager, she has since expanded to leading roles in independent dramas and film franchises", new DateTime(1989, 10, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brianne", "Woman", "Desaulniers", "Sidonie", "https://www.gstatic.com/tv/thumb/persons/230638/230638_v9_ba.jpg", "Sacremento" }
                });

            migrationBuilder.InsertData(
                table: "CharacterModels",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 4, "Ms. Marvel,Carol Denvers", "Captain Marvel", "Woman", "https://en.wikipedia.org/wiki/Captain_Marvel_(film)#/media/File:Captain_Marvel_poster.jpg" },
                    { 5, "Peter Parker", "Spider-Man", "Man", "https://upload.wikimedia.org/wikipedia/en/2/21/Web_of_Spider-Man_Vol_1_129-1.png" },
                    { 6, "Colonel Nicholas Joseph Fury", "Nick Fury", "Man", "https://cdn3.movieweb.com/i/article/MnBIKHAdoI4tWtz67Q1p8fkwI7yAEG/1200:100/Marvel-Movies-Mcu-Samuel-L-Jackson-Legal-Obligations.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Descripton", "Name" },
                values: new object[] { 1, "American media franchise and shared universe centered on a series of superhero films", "Marvel Universe" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[] { 2, "Anna Boden,Ryan Fleck", 1, "Action,Superhero,Science Fiction,Fantasy", "Captain Marvel", "https://www.marvel.com/movies/captain-marvel", 2019, "https://www.youtube.com/watch?v=Z1BCujX3pw8" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[] { 3, "Jon Watts", 1, "Action,Superhero,Science Fiction,Fantasy,Comedy,Teen", "Spider-Man: Far From Home", "https://en.wikipedia.org/wiki/Spider-Man:_Far_From_Home#/media/File:Spider-Man_Far_From_Home_poster.jpg", 2019, "https://www.youtube.com/watch?v=Nt9L1jCKGnE" });

            migrationBuilder.InsertData(
                table: "MovieCharacters",
                columns: new[] { "ActorId", "CharacterId", "MovieId", "Picture" },
                values: new object[,]
                {
                    { 9, 4, 2, null },
                    { 8, 6, 2, null },
                    { 7, 5, 3, null },
                    { 8, 6, 3, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_CharacterId",
                table: "MovieCharacters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacters_MovieId",
                table: "MovieCharacters",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacters");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "CharacterModels");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
