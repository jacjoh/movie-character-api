﻿using AutoMapper;
using MoviesAPI.DTOs.Character;
using MoviesAPI.DTOs.Movie;
using MoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<MovieModel, MovieDto>().ReverseMap();

            //Manual configure the attributes
            CreateMap<MovieModel, MovieFranchiseDto>().ForMember(m => m.FranchiseName, opt => opt.MapFrom(m => m.Franchise.Name));
        }
    }
}
