﻿using AutoMapper;
using MoviesAPI.DTOs.Actor;
using MoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<ActorModel, ActorDto>().ReverseMap();
        }
    }
}
