﻿using AutoMapper;
using Microsoft.CodeAnalysis;
using MoviesAPI.DTOs.Franchise;
using MoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseModel, FranchiseDto>().ReverseMap();
            CreateMap<FranchiseModel, AllFranchisesDto>().ReverseMap();
        }
    }
}
