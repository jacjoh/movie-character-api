﻿using AutoMapper;
using MoviesAPI.DTOs.MovieCharacter;
using MoviesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            //Manual configure the attributes
            CreateMap<MovieCharacterModel, MovieCharacterDto>().ForMember(mcdto => mcdto.Movie, opt => opt.MapFrom(m => m.Movie.MovieTitle))
                .ForMember(mcdto => mcdto.Character, opt => opt.MapFrom(m => m.Character.FullName))
                .ForMember(mcdto => mcdto.Actor, opt => opt.MapFrom(m => (m.Actor.FirstName+" "+m.Actor.LastName)));

            CreateMap<MovieCharacterModel, MovieCharacterPostDto>().ReverseMap();

            //Manual configure the attributes
            CreateMap<MovieCharacterModel, CharacterForMovieDto>()
                .ForMember(mcdto => mcdto.Character, opt => opt.MapFrom(m => m.Character.FullName))
                .ForMember(mcdto => mcdto.Actor, opt => opt.MapFrom(m => (m.Actor.FirstName + " " + m.Actor.LastName)));
        }
    }
}
